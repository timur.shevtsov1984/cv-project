<?php

namespace Tests\Unit;

use Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;

class DatabaseConnectionTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Тест на подключение к базе данных.
     *
     * @return void
     */
    public function test_database_connection(): void
    {
        try {
            $result = DB::select('SELECT 1');
            $this->assertEquals(1, $result[0]->{'1'});
        } catch (Exception $e) {
            $this->fail('Не удалось подключиться к базе данных: ' . $e->getMessage());
        }
    }
}
